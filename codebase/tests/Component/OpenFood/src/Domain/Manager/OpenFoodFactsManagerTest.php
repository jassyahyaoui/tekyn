<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Tests\Component\OpenFood\src\Domain\Manager;

use App\Component\Common\Services\CacheData;
use App\Component\Common\Services\CacheKeys;
use App\Component\OpenFood\src\Domain\Manager\OpenFoodFactsManager;
use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;
use App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts\OpenFoodFactsRepository;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Class OpenFoodFactsManagerTest
 */
class OpenFoodFactsManagerTest extends TestCase
{
    use ProphecyTrait;
    /**
     * @var OpenFoodFactsRepository|\Prophecy\Prophecy\ObjectProphecy
     */
    private $repository;
    /**
     * @var CacheData|\Prophecy\Prophecy\ObjectProphecy
     */
    private $cacheData;
    /**
     * @var OpenFoodFactsManager
     */
    private OpenFoodFactsManager $manager;

    protected function setUp(): void
    {
        $this->repository = $this->prophesize(OpenFoodFactsRepository::class);
        $this->cacheData = $this->prophesize(CacheData::class);

        $this->cacheData->getKey(Argument::any())->willReturn('key_1');
        $this->cacheData->saveData(Argument::any(), Argument::any())->shouldBeCalled();
        $this->cacheData->saveKeys(Argument::any())->shouldBeCalled();
        $this->manager = new OpenFoodFactsManager(
            $this->repository->reveal(),
            $this->cacheData->reveal()
        );
    }

    public function testSearchProductByTerm(): void
    {
        $productCollection = new ProductCollection();
        $product = new Product();
        $product->setEan('1234567890123');
        $product->setName('pdt 1');
        $product->setBrand('pdt brand');
        $productCollection->add($product);
        $this->repository->SearchProductByTerm('biscuit', '', '', '')
            ->willReturn($productCollection)
            ->shouldBeCalled();
        $this->manager->SearchProductByTerm('biscuit', '', '', '');
    }
}
