<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Tests\Component\OpenFood\src\Application\Action;

use App\Component\OpenFood\src\Application\Action\SearchAction;
use App\Component\OpenFood\src\Application\DTO\Request\SearchRequest;
use App\Component\OpenFood\src\Application\DTO\Response\ProductCollectionResponse;
use App\Component\OpenFood\src\Domain\Manager\OpenFoodFactsManager;
use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;

class SearchActionTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @var OpenFoodFactsManager|\Prophecy\Prophecy\ObjectProphecy
     */
    private $foodManager;
    /**
     * @var SearchRequest|\Prophecy\Prophecy\ObjectProphecy
     */
    private $searchRequest;
    /**
     * @var SearchAction
     */
    private SearchAction $action;

    protected function setUp(): void
    {
        $this->foodManager = $this->prophesize(OpenFoodFactsManager::class);
        $this->searchRequest = $this->prophesize(SearchRequest::class);
        $this->action = new SearchAction($this->foodManager->reveal());
    }

    public function testExecute(): void
    {
        $productCollection = new ProductCollection();
        $product = new Product();
        $product->setEan('1234567890123');
        $product->setName('lindt');
        $product->setBrand('lindt');
        $productCollection->add($product);

        $this->searchRequest->getTerm()->willReturn('chocolat');
        $this->searchRequest->getBrands()->willReturn('lindt');
        $this->searchRequest->getAllergens()->willReturn('');
        $this->searchRequest->getCategories()->willReturn('');

        $this->foodManager->SearchProductByTerm('chocolat', 'lindt', '', '')->willReturn($productCollection);

        $response = $this->action->execute($this->searchRequest->reveal());
        $this->assertInstanceOf(ProductCollectionResponse::class, $response);
    }
}
