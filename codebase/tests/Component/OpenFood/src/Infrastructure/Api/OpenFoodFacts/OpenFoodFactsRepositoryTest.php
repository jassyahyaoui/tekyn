<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Tests\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts;

use App\Component\Common\Services\CacheData;
use App\Component\Common\Services\DataHelper;
use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;
use App\Component\OpenFood\src\Infrastructure\Api\Normalizer\OpenFoodFactsDenormalizer;
use App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts\OpenFoodFactsRepository;
use Doctrine\ORM\EntityManagerInterface;
use http\Client\Response;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class OpenFoodFactsRepositoryTest
 */
class OpenFoodFactsRepositoryTest extends TestCase
{

    use ProphecyTrait;
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy|HttpClient
     */
    private $client;
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy|SerializerInterface
     */
    private $serializer;
    /**
     * @var OpenFoodFactsRepository
     */
    private OpenFoodFactsRepository $repository;
    /**
     * @var \Prophecy\Prophecy\ObjectProphecy|ResponseInterface
     */
    private $response;
    /**
     * @var CacheData|\Prophecy\Prophecy\ObjectProphecy
     */
    private $cacheData;
    /**
     * @var DataHelper|\Prophecy\Prophecy\ObjectProphecy
     */
    private $helper;
    /**
     * @var EntityManagerInterface|\Prophecy\Prophecy\ObjectProphecy
     */
    private $em;


    protected function setUp(): void
    {
        $this->response = $this->prophesize(ResponseInterface::class);
        $this->client = $this->prophesize(HttpClientInterface::class);
        $this->serializer = $this->prophesize(OpenFoodFactsDenormalizer::class);
        $this->cacheData = $this->prophesize(CacheData::class);
        $this->cacheData->getKey(Argument::any())->willReturn('key_1');
        $this->cacheData->hasKey(Argument::any())->willReturn(false);
        $this->helper = $this->prophesize(DataHelper::class);
        $this->em = $this->prophesize(EntityManagerInterface::class);
        $this->repository = new OpenFoodFactsRepository(
            $this->client->reveal(),
            $this->serializer->reveal(),
            $this->cacheData->reveal(),
            $this->helper->reveal(),
            $this->em->reveal()
        );
    }

    public function testSearchProductByTerm(): void
    {
        $this->response->toArray()->willReturn([
            'products' => [
                'pdt 1' => [
                    'ean' => '1231231231231',
                ],
            ],
        ]);
        $url = "https://fr.openfoodfacts.org/cgi/search.pl?search_terms=chocolat&json=1";
        $this->client->request(
            'GET',
            $url
        )->shouldBeCalled()->willReturn($this->response->reveal());

        $this->repository->SearchProductByTerm('chocolat', '', '', '');
    }
}
