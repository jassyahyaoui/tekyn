<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AbstractApiController extends AbstractController
{
    /**
     * @param $response
     * @param $page
     * @param $limit
     * @param $route
     *
     * @return array
     */
    public function paginate($response, $page, $limit, $route): array
    {
        $basicRoute = $this->generateUrl($route);
        $offset = ($page - 1) * $limit;
        $total_items = count($response);
        $last = (int)ceil($total_items / $limit);
        $paginatedResponse = array_splice($response, $offset, $limit);

        $next = $last - $page >= 1 ? $page + 1 : null;
        $previous = $page > 1 ? $page - 1 : null;

        return [
            'data' => $paginatedResponse,
            'links' => [
                "self" => $page <= 1 ? $basicRoute : sprintf($basicRoute . "?page=%s", $page),
                "next" => isset($next) ? sprintf($basicRoute . "?page=%s", $next) : null,
                "previous" => isset($previous) ? sprintf($basicRoute . "?page=%s", $previous) : null,
                "last" => $last !== 0 ? sprintf($basicRoute . "?page=%s", $last) : null,
            ],
        ];
    }
}
