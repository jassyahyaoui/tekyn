<?php

namespace App\Controller;

use App\Component\OpenFood\src\Application\Action\SaveAction;
use App\Component\OpenFood\src\Application\Action\SearchAction;
use App\Component\OpenFood\src\Application\DTO\Request\SearchRequest;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractApiController
{
    /**
     * @Route("/api/search/name", name="api_search")
     *
     * @param Request      $request
     * @param SearchAction $action
     *
     * @return JsonResponse
     */
    public function search(Request $request, SearchAction $action): JsonResponse
    {
        $response = $action->execute(new SearchRequest(
                $request->query->get('term', ''),
                $request->query->get('brands', ''),
                $request->query->get('allergens', ''),
                $request->query->get('categories', '')
            )
        )->getData();

        $response = $this->paginate(
            $response,
            (int)$request->query->get('page', 1),
            (int)$request->query->get('limit', 20),
            $request->attributes->get('_route')
        );

        return $this->json($response, Response::HTTP_OK);
    }

    /**
     * @Route("/api/save/ean_du_produit", name="api_save" , methods={"POST"})
     *
     * @param SaveAction $action
     *
     * @return JsonResponse
     */
    public function save(SaveAction $action): JsonResponse
    {
        $action->execute();

        return $this->json('created', Response::HTTP_CREATED);
    }
}
