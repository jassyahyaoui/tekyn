<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\Common\Services;

use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;

/**
 * Class DataHelper
 */
class DataHelper
{
    /**
     * @param string $json
     *
     * @return ProductCollection
     */
    public function convertToObject(string $json): ProductCollection
    {
        $arr = json_decode($json, true);
        $data = array_shift($arr);

        return $this->fillDataInCollection($data);
    }

    /**
     * @param array $data
     *
     * @return ProductCollection
     */
    public function fillDataInCollection(array $data): ProductCollection
    {
        $products = new ProductCollection();
        foreach ($data as $index => $item) {
            ${"product_$index"} = $this->fillDataInObject($data, $index);

            $products->add(${"product_$index"});
        }

        return $products;
    }

    public function fillDataInObject(array $item, string $index): Product
    {
        ${"product_$index"} = new Product();
        ${"product_$index"}->setEan($item['code'] ?? $item['ean'] ?? '');
        ${"product_$index"}->setName($item['product_name'] ?? $item['name'] ?? '');
        ${"product_$index"}->setBrand($item['brands'] ?? $item['brand'] ?? '');
        ${"product_$index"}->setIngredients($item['ingredients_text'] ?? $item['ingredients'] ?? '');
        ${"product_$index"}->setAllergens($item['allergens'] ?? $item['allergens'] ?? '');
        ${"product_$index"}->setNutriScore($item['nutriscore_grade'] ?? $item['nutriScore'] ?? '');
        ${"product_$index"}->setNutriValue($item['nutriscore_data']['energy'] ?? $item['nutriValue'] ?? '');

        return ${"product_$index"};
    }
}
