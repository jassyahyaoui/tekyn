<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\Common\Services;

use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Class CacheData
 */
class CacheData
{
    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;
    /**
     * @var SerializerInterface
     */
    private SerializerInterface $serializer;

    /**
     * CacheData constructor.
     *
     * @param CacheInterface      $cache
     * @param SerializerInterface $serializer
     */
    public function __construct(CacheInterface $cache, SerializerInterface $serializer)
    {
        $this->cache = $cache;
        $this->serializer = $serializer;
    }

    /**
     * @param string $key
     * @param object $response
     */
    public function saveData(string $key, object $response): void
    {
        $jsonContent = $this->serializer->serialize($response, 'json');
        $item = $this->cache->getItem($key);

        if (!$item->isHit()) {
            $item->set($jsonContent);
            $this->cache->save($item);
        }
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function getData(string $key)
    {
        $item = $this->cache->getItem($key);

        return $item->get();
    }

    /**
     * @param array $criterias
     *
     * @return string
     */
    public function getKey(array $criterias): string
    {
        $key = 'key_';
        foreach ($criterias as $criteria) {
            if ('' == $criteria) {
                continue;
            }
            $key .= trim(md5($criteria));
        }

        return $key;
    }


    /**
     * @param string $searchedTerm
     */
    public function saveKeys(string $searchedTerm): void
    {
        $allKeys = [];
        $cacheKeys = $this->cache->getItem('cache_keys');
        if (null !== $cacheKeys->get()) {
            $allKeys += $cacheKeys->get();
        }

        if (!\in_array($searchedTerm, $allKeys)) {
            array_push($allKeys, $searchedTerm);
        }

        $cacheKeys->set($allKeys);
        $this->cache->save($cacheKeys);
    }

    /**
     * @return array
     */
    public function getAllKeys(): array
    {
        $cacheKeys = $this->cache->getItem('cache_keys');

        return $cacheKeys->get() ?? [];
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasKey(string $key): bool
    {
        return empty($this->getAllKeys()) ? false : \in_array($key, $this->getAllKeys());
    }
}
