<?php

namespace App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts;

/**
 * Interface OpenFoodFactsRepositoryInterface
 *
 * @package App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts
 */
interface OpenFoodFactsRepositoryInterface
{
    /**
     * @param string $term
     * @param string $brands
     * @param string $allergens
     * @param string $categories
     *
     * @return mixed
     */
    public function SearchProductByTerm(string $term, string $brands, string $allergens, string $categories);

    public function saveProduct(): void ;
}
