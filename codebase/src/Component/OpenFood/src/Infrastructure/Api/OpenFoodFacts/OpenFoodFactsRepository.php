<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts;

use App\Component\Common\Services\CacheData;
use App\Component\Common\Services\DataHelper;
use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Infrastructure\Api\Normalizer\OpenFoodFactsDenormalizer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class OpenFoodFactsRepository
 */
class OpenFoodFactsRepository implements OpenFoodFactsRepositoryInterface
{
    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;
    /**
     * @var OpenFoodFactsDenormalizer
     */
    private OpenFoodFactsDenormalizer $serializer;
    /**
     * @var CacheData
     */
    private CacheData $cacheData;
    /**
     * @var DataHelper
     */
    private DataHelper $helper;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    /**
     * OpenFoodFactsRepository constructor.
     *
     * @param HttpClientInterface       $client
     * @param OpenFoodFactsDenormalizer $serializer
     * @param CacheData                 $cacheData
     * @param DataHelper                $helper
     * @param EntityManagerInterface    $entityManager
     */
    public function __construct(
        HttpClientInterface $client,
        OpenFoodFactsDenormalizer $serializer,
        CacheData $cacheData,
        DataHelper $helper,
        EntityManagerInterface $entityManager
    ) {
        $this->client = $client;
        $this->serializer = $serializer;
        $this->cacheData = $cacheData;
        $this->helper = $helper;
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $term
     * @param string $brands
     * @param string $allergens
     * @param string $categories
     *
     * @return \App\Component\OpenFood\src\Domain\Model\ProductCollection|mixed
     * @throws \Throwable
     */
    public function SearchProductByTerm(string $term, string $brands, string $allergens, string $categories)
    {
        $url = sprintf("https://fr.openfoodfacts.org/cgi/search.pl?search_terms=%s&json=1", $term);
        '' === $brands ?: $url .= sprintf("&brands=%s", $brands);
        '' === $allergens ?: $url .= sprintf("&allergens=%s", $allergens);
        '' === $categories ?: $url .= sprintf("&categories=%s", $categories);
        try {
            $response = $this->client->request('GET', $url);

            $key = $this->cacheData->getKey([$term, $brands, $allergens, $categories]);
            if ($this->cacheData->hasKey($key)) {
                $json = $this->cacheData->getData($key);

                return $this->helper->convertToObject($json);
            }

            return $this->serializer->denormalize($response->toArray()['products'], Product::class, null);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    public function saveProduct(): void
    {
        $arr = [];
        $allKeys = $this->cacheData->getAllKeys();
        if (empty($allKeys)) {
            throw new NotFoundHttpException('no existant data to persist from file cache');
        }

        foreach ($allKeys as $index => $key) {
            ${"cache_$index"} = $this->cacheData->getData($key);
            $arr += json_decode(${"cache_$index"}, true)['products'];
        }
        foreach ($arr as $index => $item) {
            ${"product_$index"} = $this->helper->fillDataInObject($item, $index);
            $this->entityManager->persist(${"product_$index"});
        }

        try {
            $this->entityManager->flush();
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }
}
