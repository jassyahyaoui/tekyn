<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Infrastructure\Api\Normalizer;

use App\Component\Common\Services\DataHelper;
use App\Component\OpenFood\src\Domain\Model\Product;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;
use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Contracts\Cache\CacheInterface;

/**
 * Class OpenFoodFactsDenormalizer
 */
class OpenFoodFactsDenormalizer implements DenormalizerInterface, CacheableSupportsMethodInterface
{
    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;
    /**
     * @var DataHelper
     */
    private DataHelper $helper;

    /**
     * OpenFoodFactsDenormalizer constructor.
     *
     * @param CacheInterface $cache
     */
    public function __construct(CacheInterface $cache, DataHelper $helper)
    {
        $this->cache = $cache;
        $this->helper = $helper;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return ProductCollection|mixed
     */
    public function denormalize($data, string $type, string $format = null, array $context = [])
    {
        return $this->helper->fillDataInCollection($data);
    }

    public function supportsDenormalization($data, string $type, string $format = null)
    {
        return Product::class === $type;
    }

    public function hasCacheableSupportsMethod(): bool
    {
        return true;
    }
}
