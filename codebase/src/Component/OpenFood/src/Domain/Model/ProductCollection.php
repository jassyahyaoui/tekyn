<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Domain\Model;

/**
 * Class ProductCollection
 */
class ProductCollection
{
    /**
     * @var Product[]
     */
    protected array $products;

    /**
     * @param Product[] $products
     */
    public function __construct(array $products = [])
    {
        $this->setProducts($products);
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @param array $products
     *
     * @return $this
     */
    public function setProducts(array $products): self
    {
        $this->products = [];

        foreach ($products as $product) {
            $this->add($product);
        }

        return $this;
    }

    /**
     * @param Product $product
     *
     * @return $this
     */
    public function add(Product $product): self
    {
        $this->products[$product->getEan()] = $product;

        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function remove(string $id): self
    {
        if ($this->has($id)) {
            unset($this->products[$id]);
        }

        return $this;
    }

    /**
     * @param string $id
     *
     * @return Product|mixed|null
     */
    public function get(string $id)
    {
        return $this->products[$id] ?? null;
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    public function has(string $id): bool
    {
        return isset($this->products[$id]);
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return $this->products;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return \count($this->products);
    }

    /**
     * @return Product|null
     */
    public function first()
    {
        reset($this->products);

        return current($this->products) ?? null;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator(): \ArrayIterator
    {
        return new \ArrayIterator($this->products);
    }
}
