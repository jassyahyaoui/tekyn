<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Component\OpenFood\src\Domain\Model;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="Product")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $ean;

    /**
     * @ORM\Column(type="string")
     */
    private string $brand;

    /**
     * @ORM\Column(type="text")
     */
    private string $ingredients;

    /**
     * @ORM\Column(type="text")
     */
    private string $allergens;

    /**
     * @ORM\Column(type="string")
     */
    private string $nutriScore;

    /**
     * @ORM\Column(type="integer")
     */
    private string $nutriValue;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @return string
     */
    public function getEan(): string
    {
        return $this->ean;
    }

    /**
     * @param string $ean
     */
    public function setEan(string $ean): void
    {
        $this->ean = $ean;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getIngredients(): string
    {
        return $this->ingredients;
    }

    /**
     * @param string $ingredients
     */
    public function setIngredients(string $ingredients): void
    {
        $this->ingredients = $ingredients;
    }

    /**
     * @return string
     */
    public function getAllergens(): string
    {
        return $this->allergens;
    }

    /**
     * @param string $allergens
     */
    public function setAllergens(string $allergens): void
    {
        $this->allergens = $allergens;
    }

    /**
     * @return string
     */
    public function getNutriScore(): string
    {
        return $this->nutriScore;
    }

    /**
     * @param string $nutriScore
     */
    public function setNutriScore(string $nutriScore): void
    {
        $this->nutriScore = $nutriScore;
    }

    /**
     * @return string
     */
    public function getNutriValue(): string
    {
        return $this->nutriValue;
    }

    /**
     * @param string $nutriValue
     */
    public function setNutriValue(string $nutriValue): void
    {
        $this->nutriValue = $nutriValue;
    }

    /**
     * @return string
     */
    public function getBrand(): string
    {
        return $this->brand;
    }

    /**
     * @param string $brand
     */
    public function setBrand(string $brand): void
    {
        $this->brand = $brand;
    }
}
