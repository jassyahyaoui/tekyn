<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Domain\Manager;

use App\Component\Common\Services\CacheData;
use App\Component\Common\Services\CacheKeys;
use App\Component\OpenFood\src\Domain\Model\ProductCollection;
use App\Component\OpenFood\src\Infrastructure\Api\OpenFoodFacts\OpenFoodFactsRepositoryInterface;

/**
 * Class OpenFoodFactsManager
 */
class OpenFoodFactsManager implements OpenFoodFactsManagerInterface
{
    /**
     * @var OpenFoodFactsRepositoryInterface
     */
    private OpenFoodFactsRepositoryInterface $openFoodFactsRepository;
    /**
     * @var CacheData
     */
    private CacheData $cacheData;

    /**
     * OpenFoodFactsManager constructor.
     *
     * @param OpenFoodFactsRepositoryInterface $openFoodFactsRepository
     * @param CacheData                        $cacheData
     */
    public function __construct(
        OpenFoodFactsRepositoryInterface $openFoodFactsRepository,
        CacheData $cacheData
    ) {
        $this->openFoodFactsRepository = $openFoodFactsRepository;
        $this->cacheData = $cacheData;
    }

    /**
     * @param string $term
     * @param string $brands
     * @param string $allergens
     * @param string $categories
     *
     * @return ProductCollection
     */
    public function SearchProductByTerm(
        string $term,
        string $brands,
        string $allergens,
        string $categories
    ): ProductCollection {
        $response = $this->openFoodFactsRepository->SearchProductByTerm($term, $brands, $allergens, $categories);
        $this->cacheProductsData($response, $term, $brands, $allergens, $categories);

        return $response;
    }

    /**
     * @param ProductCollection $response
     * @param string            $term
     * @param string            $brands
     * @param string            $allergens
     * @param string            $categories
     */
    private function cacheProductsData(
        ProductCollection $response,
        string $term,
        string $brands,
        string $allergens,
        string $categories
    ): void {
        $key = $this->cacheData->getKey([$term, $brands, $allergens, $categories]);
        $this->cacheData->saveData($key, $response);
        $this->cacheData->saveKeys($key);
    }

    public function saveProduct(): void
    {
        $this->openFoodFactsRepository->saveProduct();
    }
}
