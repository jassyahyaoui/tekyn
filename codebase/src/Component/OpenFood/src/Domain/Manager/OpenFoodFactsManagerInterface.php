<?php


namespace App\Component\OpenFood\src\Domain\Manager;

use App\Component\OpenFood\src\Domain\Model\ProductCollection;

/**
 * Interface OpenFoodFactsManagerInterface
 *
 * @package App\Component\OpenFood\src\Domain\Manager
 */
interface OpenFoodFactsManagerInterface
{
    /**
     * @param string $term
     * @param string $brands
     * @param string $allergens
     * @param string $categories
     *
     * @return ProductCollection
     */
    public function SearchProductByTerm(string $term, string $brands, string $allergens, string $categories): ProductCollection ;
}
