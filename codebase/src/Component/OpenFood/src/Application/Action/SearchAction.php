<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Application\Action;

use App\Component\OpenFood\src\Application\DTO\Request\SearchRequest;
use App\Component\OpenFood\src\Application\DTO\Response\ProductCollectionResponse;
use App\Component\OpenFood\src\Domain\Manager\OpenFoodFactsManager;

/**
 * Class SearchAction
 */
class SearchAction
{
    /**
     * @var OpenFoodFactsManager
     */
    private OpenFoodFactsManager $manager;

    /**
     * GetProductByEanAction constructor.
     *
     * @param OpenFoodFactsManager $manager
     */
    public function __construct(OpenFoodFactsManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param SearchRequest $request
     *
     * @return ProductCollectionResponse
     */
    public function execute(SearchRequest $request)
    {
        $products = $this->manager->SearchProductByTerm(
            $request->getTerm(),
            $request->getBrands(),
            $request->getAllergens(),
            $request->getCategories()
        );

        return new ProductCollectionResponse($products);
    }
}
