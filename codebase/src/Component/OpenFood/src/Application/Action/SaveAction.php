<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Application\Action;

use App\Component\OpenFood\src\Domain\Manager\OpenFoodFactsManager;

/**
 * Class SaveAction
 */
class SaveAction
{
    /**
     * @var OpenFoodFactsManager
     */
    private OpenFoodFactsManager $manager;

    /**
     * GetProductByEanAction constructor.
     *
     * @param OpenFoodFactsManager $manager
     */
    public function __construct(OpenFoodFactsManager $manager)
    {
        $this->manager = $manager;
    }

    public function execute(): void
    {
        $this->manager->saveProduct();
    }
}
