<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Application\DTO\Response;

use App\Component\OpenFood\src\Domain\Model\Product;

/**
 * Class ProductResponse
 */
class ProductResponse
{
    /**
     * @var Product
     */
    private Product $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @param array $context
     *
     * @return array
     */
    public function getData(array $context = []): array
    {
        return [
                'id' => null,
                'type' => 'product',
                'attributes' => [
                    'ean' => $this->product->getEan(),
                    'brand' => $this->product->getName(),
                    'ingredients' => $this->product->getIngredients(),
                    'allergens' => $this->product->getAllergens(),
                    'nutriScore' => $this->product->getNutriScore(),
                ],
        ];
    }
}
