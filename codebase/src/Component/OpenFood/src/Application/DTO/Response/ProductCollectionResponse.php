<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Component\OpenFood\src\Application\DTO\Response;

use App\Component\OpenFood\src\Domain\Model\ProductCollection;

/**
 * Class SearchResponse
 */
class ProductCollectionResponse
{
    /**
     * @var ProductCollection
     */
    private ProductCollection $productCollection;

    /**
     * SearchResponse constructor.
     *
     * @param ProductCollection $productCollection
     */
    public function __construct(ProductCollection $productCollection)
    {
        $this->productCollection = $productCollection;
    }

    /**
     * @param array $context
     *
     * @return array
     */
    public function getData(array $context = []): array
    {
        $data = [];
        foreach ($this->productCollection->all() as $product) {
            $data[] = (new ProductResponse($product))->getData();
        }

        return $data;
    }
}
