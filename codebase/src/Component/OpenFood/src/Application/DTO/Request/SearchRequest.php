<?php

/*
 * This file is part of the FrontOne package.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace App\Component\OpenFood\src\Application\DTO\Request;

use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SearchRequest
 */
class SearchRequest
{
    private string $term;
    private string $brands;
    private string $allergens;
    private string $categories;

    /**
     * SearchProductByTermRequest constructor.
     *
     * @param string $term
     * @param string $brands
     * @param string $allergens
     * @param string $categories
     */
    public function __construct(string $term, string $brands, string $allergens, string $categories)
    {
        $this->resolve(['term' => $term]);
        $this->term = $term;
        $this->brands = $brands;
        $this->allergens = $allergens;
        $this->categories = $categories;
    }

    /**
     * @return string
     */
    public function getTerm(): string
    {
        return $this->term;
    }

    /**
     * @return string
     */
    public function getBrands(): string
    {
        return $this->brands;
    }

    /**
     * @return string
     */
    public function getAllergens(): string
    {
        return $this->allergens;
    }

    /**
     * @return string
     */
    public function getCategories(): string
    {
        return $this->categories;
    }


    private function resolve(array $data): array
    {
        $resolver = new OptionsResolver();

        $resolver
            ->setRequired(['term'])
            ->setAllowedTypes('term', ['string'])
            ->setAllowedValues('term', function ($value) {
                if ($value == '' || $value == null) {
                    throw new InvalidOptionsException('term field is mandatory');
                }

                return true;
            })
        ;

        return $resolver->resolve($data);
    }
}
